# American Red Cross ⛑ | **Public View**
- **National Headquarters (NHQ)** 
- **@redcross.org ⛑🩸🅰️🅱️🅾️**

## Information Security 
- **⛑ redcross.org two-factor authentication:**
- ***-> ⛑ https://secureauth.redcross.org**
- ***-> ⛑ https://sso.redcross.org**

> **⛑ redcross.org Privacy Policy**

- **⛑ https://www.redcross.org/privacy-policy.html**
