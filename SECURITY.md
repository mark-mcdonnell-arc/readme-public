# redcross.org Security Policy

## Supported Versions

**Project Red Cloud Lake**

| Version | Supported          |
| ------- | ------------------ |
| 0.0.1   | :white_check_mark: |


## Reporting a Vulnerability

Immediately report any security vulnerabilities to the IT Service Desk <itsd@redcross.org>.


